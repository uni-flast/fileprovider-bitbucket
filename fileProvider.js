console.log("github fileProvider");

const MODULE_NAME = "fileProvider";

const CLIENT_ID = "LqvDYdufTjkUhPBYNE";

let user;

let token = location.hash.substr(14).split("&")[0];

let bc = new BroadcastChannel('token_channel');

//is the redirected new tab and has code?
if(token) {
    bc.postMessage(token);
    window.close();
}
//is the original tab and has token saved
else if(sessionStorage.getItem("token")) {
    token = sessionStorage.getItem("token");
    getUser(getUserProjects);
}

bc.onmessage = function (e) { 
    token = decodeURIComponent(e.data);
    sessionStorage.setItem("token",token);
    getUser(getUserProjects);
}

document.getElementById("btnLogin").addEventListener("click",e => {
    let w = window.open( `https://bitbucket.org/site/oauth2/authorize?client_id=${CLIENT_ID}&response_type=token`,'_blank');
    w.focus();
});


function getUser(callback) {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){
        user = JSON.parse(this.responseText);
        if(callback)
            callback();
    });
    oReq.open("GET", `https://bitbucket.org/api/2.0/user`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
}

function getUserProjects() {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){console.log(this.responseText)});
    oReq.open("GET", `https://bitbucket.org/api/2.0/repositories/${user.username}`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
}

function getBranchesInRepository() {
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", getBranchesInProjectListener);
    oReq.open("GET", `https://bitbucket.org/api/2.0/repositories/${owner}/${repo}/refs/branches`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
}

function getBranchesInProjectListener() {
    console.log(this.responseText);
    let select = document.getElementById("selectBranch");
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
    let option = document.createElement("option");
    option.value = "--select branch--";
    option.text = "--select branch--";
    select.appendChild(option);
    JSON.parse(this.responseText).values.forEach(branch => {
        let option = document.createElement("option");
        option.value = branch.name;
        option.text = branch.name;
        select.appendChild(option);
    });
}

let selectBranch = document.getElementById("selectBranch");
selectBranch.addEventListener("change", e => {
    if(selectBranch.firstChild.value === "--select branch--")
        selectBranch.removeChild(selectBranch.firstChild);
    emitEvent("fileProviderReady",{branch:selectBranch.value});
});

let selectRepo = document.getElementById("btnRepoSelection");
selectRepo.addEventListener("click", e => {
    getBranchesInRepository();
});

cmds["getFile"] = getFile;
function getFile(m) {
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    const branch = document.getElementById("selectBranch").value;
    const path = m.file_path || "";
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function() {
        getFileCallback(m,this.responseText);
    });
    oReq.open("GET", `https://bitbucket.org/api/2.0/repositories/${owner}/${repo}/src/${branch}/${encodeURIComponent(path)}`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);    
    oReq.send();
}

function getFileCallback(m,res) {
    //might want to pass more info about file, ie last modified...
    m.callback.data = {content:res};
    messageModule(m.callback,m.callback.target);
}

cmds["getFilesInDirectory"] = getFilesInDirectory;
function getFilesInDirectory(m) {
    let path = m.path || "";
    const branch = document.getElementById("selectBranch").value;
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){console.log(this.responseText);getFilesInDirectoryListener(m,container,this.responseText);});
    oReq.open("GET", `https://bitbucket.org/api/2.0/repositories/${owner}/${repo}/src/${branch}/${encodeURIComponent(path)}`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
}

let fileList = [];

function getFilesInDirectoryListener (m,container,json) {
    let files = JSON.parse(json).values;
    files.forEach(f => {
        fileList.push(f);
        f.originalPath = f.path;
        f.name = f.path.split("/").pop();
        if(f.type === "commit_directory")
            f.type = "tree";
        else if(f.type === "commit_file")
            f.type = "blob";
    });
    m.callback.data = {files:files};
    messageModule(m.callback,m.callback.target);
}

cmds["commit"] = commit;
function commit(m) {
    const owner = document.getElementById("inputOwner").value;
    const repo = document.getElementById("inputRepo").value;
    const branch = m.branch || document.getElementById("selectBranch").value;
    const message = m.commit_message;
    commitTime = Date.now();

    var formData = new FormData();
    formData.append("message", message);
    formData.append("branch", branch);
    m.actions.forEach(action => {
        if(action.action === "delete") {
            formData.append("files", action.file_path);
        }
        else if(action.action === "create" || action.action === "update" || action.action === "move") {
            formData.append("/"+action.file_path,action.content);
            if(action.action === "move") {
            formData.append("files", action.previous_path);
            }
        }
    });
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){
            emitEvent("confirmCommit",{res:this.responseText,commitNumber:m.commitNumber,doMergeRequest:m.doMergeRequest});
    });
    oReq.addEventListener("error", e => emitEvent("commitError",{error:e,commitNumber:m.commitNumber}));
    oReq.open("POST", `https://bitbucket.org/api/2.0/repositories/${owner}/${repo}/src`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send(formData);
        
}














//from https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
function b64DecodeUnicode(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
}